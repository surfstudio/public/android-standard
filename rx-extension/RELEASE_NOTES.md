# Rx-extension Release Notes

- [0.5.0-alpha](#050-alpha)

## 0.5.0-alpha
##### Rx-extension
* ANDDEP-1171 Added documentation and computation() method for SchedulersProvider
* ANDDEP-671 Add Any.toObservable() extension method
* Add scheduler which runs code immediate when in main thread